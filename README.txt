Build using:

gradle build

This will build it and run the junit tests.

To run it use:

gradle run --args='<filename>'

where <filename> is the name of the file to process.